#!/bin/bash

set -ae
source .env

export RESTIC_PASSWORD_FILE="$PWD/db-restic-pwdfile"
export RESTIC_CACHE_DIR="/home/max/.cache/restic"

echo --- Backing up... ---

mariadb-dump --defaults-file="./db-backup.cnf" --single-transaction --flush-logs --events --routines --all-databases  | restic -r "$BACKUPS_ROOT/db" backup --stdin --stdin-filename backup.sql

echo --- Removing old backups ---

restic -r "$BACKUPS_ROOT/db" forget --keep-within 14d --prune --max-repack-size 0

echo --- Backup complete ---