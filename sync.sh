#!/bin/bash

set -ae
source .env

./backup-db.sh

rclone sync "$BACKUPS_ROOT" "b2:$BUCKET_NAME/" --progress --fast-list --retries 1 --log-file "$PWD/backup.log" --log-level ERROR || echo Backblaze failed

backup_size=$(du -bs "$BACKUPS_ROOT" | grep -oE "[0-9]+")
used=$(curl "$USAGE_ENDPOINT")
usage=$((backup_size + used))

if (( usage > STORAGE_QUOTA  )); then
  echo "Error: storage quota would be exceeded. Aborting sync."
  exit 1
fi

rclone sync "$BACKUPS_ROOT" backups:/ --progress --delete-before
